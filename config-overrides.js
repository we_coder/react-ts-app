const { override, addWebpackAlias, addPostcssPlugins } = require('customize-cra')
const path = require('path')
module.exports = override(
	addWebpackAlias({
		'@': path.resolve(__dirname, 'src')
	}),
	addPostcssPlugins([
		require('postcss-px2rem')({ remUnit: 75 }),
		require('autoprefixer'),
		require('postcss-preset-env')({ stage: 3 })
	])
)