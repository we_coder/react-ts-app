# 项目说明

## 命令说明

### `yarn` (npm install)

安装依赖

### `yarn start` (npm start)

启动项目，默认3000端口，支持ip访问

### `yarn test` (npm run test)

单元测试

### `yarn build` (npm run build)

项目打包，如需配置打包后的路径，可修改package.json文件的"homepage"项

### `yarn eject` (npm run eject)

在项目目录下生成配置文件，适用于对项目的再配置。注：此操作是不可逆的，建议使用react-rewrited-app进行项目再配置

