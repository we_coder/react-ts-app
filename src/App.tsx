import React, { Suspense } from 'react'
import { inject, observer } from 'mobx-react'
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'
import { routes, basename } from './router'
import '@/App.scss'

const RouteMap: React.FC = (route: any) => {
	return (
		<Route
			path={route.path}
			render={(props) => (
				<Suspense fallback={<div>loading...</div>}>
					<route.component {...props} />
				</Suspense>
			)}
		/>
	)
}

const App: React.FC = () => {
	return (
		<div className="App">
			<Router basename={basename}>
				<Switch>
					{routes.map((route, i) => (<RouteMap key={i} {...route} />))}
					<Redirect to="/home" />
				</Switch>
			</Router>
		</div>
	)
}

export default inject('rootStore', 'userStore')(observer(App))
