import { useState, useEffect } from 'react'

export function useOnEnter (callback: Function = () => {}) {
	const [isFirst, setIsFirst] = useState(true)
	useEffect(() => {
		if (isFirst) {
			callback()
			setIsFirst(false)
		}
	}, [isFirst, callback])
}