import React from 'react'
import { inject, observer } from 'mobx-react'
import { useOnEnter } from '@/hooks/index'

const HomePage: React.FC = (props: any) => {
	const { userStore: store } = props
	useOnEnter(() => {
		console.log('enter')
	})
	return (
		<div className="homepage-container">
			<h1>首页</h1>
			<p>{store.state.name}</p>
		</div>
	)
}

export default inject('userStore')(observer(HomePage))