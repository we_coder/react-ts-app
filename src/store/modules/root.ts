/* eslint-disable @typescript-eslint/consistent-type-assertions */
import { observable, action } from 'mobx'

interface RootStoreStates {
	state: object
}
interface StateProps {
	[propName: string]: any
}
class RootStore implements RootStoreStates {
	@observable state = <StateProps>{
		name: '根store4'
	}

	@action
	setState (payload: object) {
		this.state = payload
	}
}

export default new RootStore()