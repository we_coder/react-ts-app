/* eslint-disable @typescript-eslint/consistent-type-assertions */
import { observable, action } from 'mobx'

interface UserStoreStates {
	state: object
}
interface StateProps {
	[propName: string]: any
}
class UserStore implements UserStoreStates {
	@observable state = <StateProps>{
		name: 'userStore'
	}

	@action
	setState (payload: object) {
		Object.assign(this.state, payload)
	}
}

export default new UserStore()