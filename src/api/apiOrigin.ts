let prefix: string = process.env.NODE_ENV === 'development' ? 'alpha' : ''
const host: string = window.location.host

if (prefix === '') {
	prefix = host.includes('-') ? host.split('-')[0] : ''
}
let API_ORIGIN: string = ''
switch (prefix) {
case 'alpha':
	API_ORIGIN = 'https://alpha-api.szy.cn'
	break
case 'rc':
	API_ORIGIN = 'https://rc-api.szy.cn'
	break
default:
	API_ORIGIN = 'https://api.szy.cn'
}
const path = (_path: string): string => API_ORIGIN + _path
export {
	path
}