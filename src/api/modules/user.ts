import { API } from '../api'
import { path } from '../apiOrigin'

export const USER_API = {
	LOGIN: path('/user/login/v1.0')
}

export default {
	async login (params?: object, config?: object) {
		return API.post(USER_API.LOGIN, params, config)
	}
}