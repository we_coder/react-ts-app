import Axios from 'axios'

const Ajax = Axios.create({
	timeout: 6000,
	headers: {
		'Content-Type': 'application/json'
	}
})

export const API = {
	async get (url: string, params: object = {}, config: object = {}): Promise<any> {
		try {
			const res = await Ajax.get(url, {
				params,
				...config
			})
			if (res.data.code === 10000 || res.data.returncode === '10000') {
				return res.data.body
			} else {
				throw res.data.message
			}
		} catch (error) {
			return Promise.reject(error)
		}
	},
	async post (url: string, params: object = {}, config: object = {}): Promise<any> {
		try {
			const res = await Ajax.post(url, params, config)
			if (res.data.code === 10000 || res.data.returncode === '10000') {
				return res.data.body
			} else {
				throw res.data.message
			}
		} catch (error) {
			return Promise.reject(error)
		}
	}
}