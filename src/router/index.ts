import React from 'react'
const HomePage = React.lazy(() => import('../views/home/HomePage'))
interface RouteVer {
	readonly path: string,
	readonly component: React.FC
	readonly name?: string
	children?: Array<RouteVer>
}

const basename = '{{basename}}'

const routes: RouteVer[] = [
	{
		path: '/home',
		name: 'HomePage',
		component: HomePage
	}
]

export {
	basename,
	routes
}